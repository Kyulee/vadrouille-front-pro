import React from "react";
import {
  Container,
  Card,
  Row,
  Col,
  ListGroup,
  Tab,
  Sonnet,
} from "react-bootstrap";

function About() {
  return (
    <>
      <Container fluid>
        <Card>
          <Card.Header>
            <Card.Title as="h4">A propos de nous</Card.Title>
          </Card.Header>
          <Card.Body>
            <Tab.Container
              id="list-group-tabs-example"
              defaultActiveKey="#link1"
            >
              <Row>
                <Col sm={4}>
                  <ListGroup>
                    <ListGroup.Item action href="#link0">
                      Nous contacter
                    </ListGroup.Item>
                    <ListGroup.Item action href="#link1">
                      RGDP
                    </ListGroup.Item>
                    <ListGroup.Item action href="#link2">
                      Mentions Légales
                    </ListGroup.Item>
                    <ListGroup.Item action href="#link3">
                      Conditions Générale de vente / d'utilisation
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col sm={8}>
                  <Tab.Content>
                    <Tab.Pane eventKey="#link0">
                      <p>
                        Pour nous contacter, vous pouvez passer par ce
                        formulaire ou passer par notre email de contact.
                      </p>
                    </Tab.Pane>
                    <Tab.Pane eventKey="#link1">
                      <p>Test test</p>
                    </Tab.Pane>
                    <Tab.Pane eventKey="#link2">
                      <p>dmskqjfklsjqfklsjqkflsq</p>
                    </Tab.Pane>
                    <Tab.Pane eventKey="#link3">
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged.
                        It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem Ipsum passages, and
                        more recently with desktop publishing software like
                        Aldus PageMaker including versions of Lorem Ipsum.
                      </p>
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
            <div style={{ marginTop: "100px", textAlign: "center" }}>
              <h6>Tous droit réservé la Vadrouille.</h6>
            </div>
          </Card.Body>
        </Card>
      </Container>
    </>
  );
}

export default About;
