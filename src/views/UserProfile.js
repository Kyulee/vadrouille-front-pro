import React from "react";
import {
  Button,
  Card,
  Form,
  Container,
  Row,
  Col,
  Image,
} from "react-bootstrap";

function User() {
  return (
    <>
      <Container fluid>
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
            <Card>
              <Card.Header>
                <Card.Title as="h4">Mon profil</Card.Title>
              </Card.Header>
              <Card.Body style={{ textAlign: "center" }}>
                <Image
                  src="https://st.depositphotos.com/1010751/4568/v/600/depositphotos_45684179-stock-illustration-people-tree-logo-for-family.jpg"
                  roundedCircle
                  style={{
                    width: "125px",
                    height: "125px",
                    border: "2px solid black",
                  }}
                  alt="ProfilePicture"
                />
                <Form>
                  <Row>
                    <Col className="pr-1" md="12">
                      <Form.Group>
                        <label>Dénomation commerciale</label>
                        <Form.Control
                          placeholder="Company"
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-1" md="12">
                      <Form.Group>
                        <label>N° SIRET</label>
                        <Form.Control
                          placeholder="Numéro de SIRET"
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-1" md="12">
                      <Form.Group>
                        <label htmlFor="exampleInputEmail1">
                          Adresse e-mail
                        </label>
                        <Form.Control
                          placeholder="Email"
                          type="email"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      <Form.Group>
                        <label>Adresse postale</label>
                        <Form.Control type="text"></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Ville</label>
                        <Form.Control
                          placeholder="Ville"
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                    <Col className="pl-1" md="6">
                      <Form.Group>
                        <label>Code Postal</label>
                        <Form.Control
                          placeholder="Code postal"
                          type="number"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      <Form.Group>
                        <label>Description</label>
                        <Form.Control
                          placeholder="Décrivez votre entreprise/association en quelques lignes"
                          rows={4}
                          as="textarea"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <a href="mailto:vadrouille@gmail.com">
                        Désactivé mon compte
                      </a>
                    </Col>
                    <Col>
                      <Button
                        className="btn-fill pull-right"
                        type="submit"
                        variant="primary"
                      >
                        Mettre à jour mon profil
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default User;
