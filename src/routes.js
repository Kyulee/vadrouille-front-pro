import React from "react";

//import Dashboard from "./views/Dashboard.js";
import UserProfile from "./views/UserProfile.js";
import Activities from "./views/Activities.js";
import Messagerie from "./views/Messagerie.js";
import Billing from "./views/Billing.js";
import About from "./views/About.js";
import Notifications from "./views/Notifications.js";

const Dashboard = React.lazy(() => import("./views/Dashboard.js"));

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-35",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/user",
    name: "Mon profil",
    icon: "nc-icon nc-circle-09",
    component: UserProfile,
    layout: "/admin",
  },
  {
    path: "/activities",
    name: "Mes activités",
    icon: "nc-icon nc-notes",
    component: Activities,
    layout: "/admin",
  },
  {
    path: "/messagerie",
    name: "Messagerie",
    icon: "nc-icon nc-email-85",
    component: Messagerie,
    layout: "/admin",
  },
  {
    path: "/billing",
    name: "Paiement & Facturation",
    icon: "nc-icon nc-credit-card",
    component: Billing,
    layout: "/admin",
  },
  {
    path: "/info",
    name: "A propos",
    icon: "nc-icon nc-settings-gear-64",
    component: About,
    layout: "/admin",
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "nc-icon nc-bell-55",
    component: Notifications,
    layout: "/admin",
  },
  {
    path: "/login",
    name: "Connexion",
  },
  {
    path: "/register",
    name: "Inscription",
  },
];

export default dashboardRoutes;
